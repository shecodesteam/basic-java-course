package SheCodes;

public class MainClass {
	
	private static void printStringDynamic() {
        /* Define number of rows and columns */
        int numOfRows = 2;
        int numOfCols = 3;
        
        /* Initiate array */
        String[][] stringsArray = new String[numOfRows][numOfCols];
        
        /* Insert values to array cells */
        for (int i = 0; i<numOfRows; i++) {
            for (int j = 0; j<numOfCols; j++) {
                stringsArray[i][j] = "row" + (i+1) + ", col" + (j+1);
            }
        }
        
        /* Print a cell */
        System.out.println("String in second row third column is: " + stringsArray[1][2]);
    }
    
    private static void printStringStatic() {
       
        /* Initiate array with values */
        String[][] stringsArray = { {"row1, col1", "row1, col2", "row1, col3"}, 
                                    {"row2, col1", "row2, col2", "row2, col3"} };
        
        /* Print a cell */
        System.out.println("String in second row third column is: " + stringsArray[1][2]);
    }

	
	public static void main(String[] args) {
		printStringDynamic();
		printStringStatic();
	}

}

