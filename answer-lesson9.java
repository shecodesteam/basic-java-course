package SheCodes;

public class Player {
	private String name;
	private int points;
	
	public String getName() {
		return name;
	}

	public int getPoints() {
		return points;
	}
	
	public void updateRoundPoints(int points) {
		this.points += points;
	}

	@Override
	public String toString() {
		return "Player [name=" + name + ", points=" + points + "]";
	}

}
